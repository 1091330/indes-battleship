﻿namespace INDES_T3
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Game));
            this.hitsLabel1 = new System.Windows.Forms.Label();
            this.hitsLabel2 = new System.Windows.Forms.Label();
            this.hitsLabel3 = new System.Windows.Forms.Label();
            this.missesLabel1 = new System.Windows.Forms.Label();
            this.missesLabel2 = new System.Windows.Forms.Label();
            this.missesLabel3 = new System.Windows.Forms.Label();
            this.timeLabel1 = new System.Windows.Forms.Label();
            this.timeLabel2 = new System.Windows.Forms.Label();
            this.timeLabel3 = new System.Windows.Forms.Label();
            this.labelInfo = new System.Windows.Forms.Label();
            this.gameTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // hitsLabel1
            // 
            this.hitsLabel1.BackColor = System.Drawing.Color.Transparent;
            this.hitsLabel1.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hitsLabel1.ForeColor = System.Drawing.Color.ForestGreen;
            this.hitsLabel1.Location = new System.Drawing.Point(650, 202);
            this.hitsLabel1.Name = "hitsLabel1";
            this.hitsLabel1.Size = new System.Drawing.Size(28, 55);
            this.hitsLabel1.TabIndex = 0;
            this.hitsLabel1.Text = "0";
            this.hitsLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hitsLabel2
            // 
            this.hitsLabel2.BackColor = System.Drawing.Color.Transparent;
            this.hitsLabel2.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hitsLabel2.ForeColor = System.Drawing.Color.ForestGreen;
            this.hitsLabel2.Location = new System.Drawing.Point(619, 202);
            this.hitsLabel2.Name = "hitsLabel2";
            this.hitsLabel2.Size = new System.Drawing.Size(27, 55);
            this.hitsLabel2.TabIndex = 1;
            this.hitsLabel2.Text = "0";
            this.hitsLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hitsLabel3
            // 
            this.hitsLabel3.BackColor = System.Drawing.Color.Transparent;
            this.hitsLabel3.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hitsLabel3.ForeColor = System.Drawing.Color.ForestGreen;
            this.hitsLabel3.Location = new System.Drawing.Point(587, 202);
            this.hitsLabel3.Name = "hitsLabel3";
            this.hitsLabel3.Size = new System.Drawing.Size(27, 55);
            this.hitsLabel3.TabIndex = 2;
            this.hitsLabel3.Text = "0";
            this.hitsLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // missesLabel1
            // 
            this.missesLabel1.BackColor = System.Drawing.Color.Transparent;
            this.missesLabel1.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.missesLabel1.ForeColor = System.Drawing.Color.ForestGreen;
            this.missesLabel1.Location = new System.Drawing.Point(650, 363);
            this.missesLabel1.Name = "missesLabel1";
            this.missesLabel1.Size = new System.Drawing.Size(28, 55);
            this.missesLabel1.TabIndex = 3;
            this.missesLabel1.Text = "0";
            this.missesLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // missesLabel2
            // 
            this.missesLabel2.BackColor = System.Drawing.Color.Transparent;
            this.missesLabel2.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.missesLabel2.ForeColor = System.Drawing.Color.ForestGreen;
            this.missesLabel2.Location = new System.Drawing.Point(618, 363);
            this.missesLabel2.Name = "missesLabel2";
            this.missesLabel2.Size = new System.Drawing.Size(28, 55);
            this.missesLabel2.TabIndex = 4;
            this.missesLabel2.Text = "0";
            this.missesLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // missesLabel3
            // 
            this.missesLabel3.BackColor = System.Drawing.Color.Transparent;
            this.missesLabel3.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.missesLabel3.ForeColor = System.Drawing.Color.ForestGreen;
            this.missesLabel3.Location = new System.Drawing.Point(587, 363);
            this.missesLabel3.Name = "missesLabel3";
            this.missesLabel3.Size = new System.Drawing.Size(28, 55);
            this.missesLabel3.TabIndex = 5;
            this.missesLabel3.Text = "0";
            this.missesLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timeLabel1
            // 
            this.timeLabel1.BackColor = System.Drawing.Color.Transparent;
            this.timeLabel1.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel1.ForeColor = System.Drawing.Color.ForestGreen;
            this.timeLabel1.Location = new System.Drawing.Point(650, 524);
            this.timeLabel1.Name = "timeLabel1";
            this.timeLabel1.Size = new System.Drawing.Size(28, 55);
            this.timeLabel1.TabIndex = 6;
            this.timeLabel1.Text = "0";
            this.timeLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timeLabel2
            // 
            this.timeLabel2.BackColor = System.Drawing.Color.Transparent;
            this.timeLabel2.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel2.ForeColor = System.Drawing.Color.ForestGreen;
            this.timeLabel2.Location = new System.Drawing.Point(619, 524);
            this.timeLabel2.Name = "timeLabel2";
            this.timeLabel2.Size = new System.Drawing.Size(28, 55);
            this.timeLabel2.TabIndex = 7;
            this.timeLabel2.Text = "0";
            this.timeLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timeLabel3
            // 
            this.timeLabel3.BackColor = System.Drawing.Color.Transparent;
            this.timeLabel3.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel3.ForeColor = System.Drawing.Color.ForestGreen;
            this.timeLabel3.Location = new System.Drawing.Point(585, 524);
            this.timeLabel3.Name = "timeLabel3";
            this.timeLabel3.Size = new System.Drawing.Size(28, 55);
            this.timeLabel3.TabIndex = 8;
            this.timeLabel3.Text = "0";
            this.timeLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelInfo
            // 
            this.labelInfo.BackColor = System.Drawing.Color.Transparent;
            this.labelInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfo.ForeColor = System.Drawing.Color.Green;
            this.labelInfo.Location = new System.Drawing.Point(378, 626);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(775, 30);
            this.labelInfo.TabIndex = 9;
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::INDES_T3.Properties.Resources.gameBase;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.timeLabel3);
            this.Controls.Add(this.timeLabel2);
            this.Controls.Add(this.timeLabel1);
            this.Controls.Add(this.missesLabel3);
            this.Controls.Add(this.missesLabel2);
            this.Controls.Add(this.missesLabel1);
            this.Controls.Add(this.hitsLabel3);
            this.Controls.Add(this.hitsLabel2);
            this.Controls.Add(this.hitsLabel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Game";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);

        }





        #endregion

        private System.Windows.Forms.Label hitsLabel1;
        private System.Windows.Forms.Label hitsLabel2;
        private System.Windows.Forms.Label hitsLabel3;
        private System.Windows.Forms.Label missesLabel1;
        private System.Windows.Forms.Label missesLabel2;
        private System.Windows.Forms.Label missesLabel3;
        private System.Windows.Forms.Label timeLabel1;
        private System.Windows.Forms.Label timeLabel2;
        private System.Windows.Forms.Label timeLabel3;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.Timer gameTimer;
    }
}
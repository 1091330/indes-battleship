﻿using System;
using System.IO;
using System.Windows.Forms;

namespace INDES_T3
{
    public partial class Help : Form
    {
        public Help()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string gamePath   = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            string manualPath = gamePath + "\\Resources"; 
            Console.WriteLine(manualPath);

            if(Directory.Exists(manualPath))
            {
                string ManualFile = manualPath + "\\Battleships Manual.pdf";
                if (File.Exists(ManualFile))
                {
                    System.Diagnostics.Process.Start(ManualFile);
                }
            }
            else
            {
                string mstTitle    = "INDES: Battleships (Help)";
                string msgError004 = "Error 004: The manual was not found.";

                DialogResult error = MessageBox.Show(msgError004, mstTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

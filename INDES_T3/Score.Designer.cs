﻿namespace INDES_T3
{
    partial class Score
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Score));
            this.hitsp1 = new System.Windows.Forms.Label();
            this.hitsp2 = new System.Windows.Forms.Label();
            this.hitsp3 = new System.Windows.Forms.Label();
            this.missesp3 = new System.Windows.Forms.Label();
            this.missesp2 = new System.Windows.Forms.Label();
            this.missesp1 = new System.Windows.Forms.Label();
            this.hpp1 = new System.Windows.Forms.Label();
            this.hpp2 = new System.Windows.Forms.Label();
            this.hpp3 = new System.Windows.Forms.Label();
            this.hitsc1 = new System.Windows.Forms.Label();
            this.hitsc2 = new System.Windows.Forms.Label();
            this.hitsc3 = new System.Windows.Forms.Label();
            this.missesc1 = new System.Windows.Forms.Label();
            this.missesc2 = new System.Windows.Forms.Label();
            this.missesc3 = new System.Windows.Forms.Label();
            this.hpc1 = new System.Windows.Forms.Label();
            this.hpc2 = new System.Windows.Forms.Label();
            this.hpc3 = new System.Windows.Forms.Label();
            this.winnerlabel = new System.Windows.Forms.Label();
            this.newGame = new System.Windows.Forms.Button();
            this.sendData = new System.Windows.Forms.Button();
            this.mainMenu = new System.Windows.Forms.Button();
            this.Quit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // hitsp1
            // 
            this.hitsp1.BackColor = System.Drawing.Color.Transparent;
            this.hitsp1.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hitsp1.ForeColor = System.Drawing.Color.ForestGreen;
            this.hitsp1.Location = new System.Drawing.Point(97, 117);
            this.hitsp1.Name = "hitsp1";
            this.hitsp1.Size = new System.Drawing.Size(23, 35);
            this.hitsp1.TabIndex = 0;
            this.hitsp1.Text = "0";
            // 
            // hitsp2
            // 
            this.hitsp2.BackColor = System.Drawing.Color.Transparent;
            this.hitsp2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hitsp2.ForeColor = System.Drawing.Color.ForestGreen;
            this.hitsp2.Location = new System.Drawing.Point(115, 117);
            this.hitsp2.Name = "hitsp2";
            this.hitsp2.Size = new System.Drawing.Size(23, 35);
            this.hitsp2.TabIndex = 1;
            this.hitsp2.Text = "0";
            // 
            // hitsp3
            // 
            this.hitsp3.BackColor = System.Drawing.Color.Transparent;
            this.hitsp3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hitsp3.ForeColor = System.Drawing.Color.ForestGreen;
            this.hitsp3.Location = new System.Drawing.Point(134, 117);
            this.hitsp3.Name = "hitsp3";
            this.hitsp3.Size = new System.Drawing.Size(23, 35);
            this.hitsp3.TabIndex = 2;
            this.hitsp3.Text = "0";
            // 
            // missesp3
            // 
            this.missesp3.BackColor = System.Drawing.Color.Transparent;
            this.missesp3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.missesp3.ForeColor = System.Drawing.Color.ForestGreen;
            this.missesp3.Location = new System.Drawing.Point(133, 213);
            this.missesp3.Name = "missesp3";
            this.missesp3.Size = new System.Drawing.Size(23, 35);
            this.missesp3.TabIndex = 3;
            this.missesp3.Text = "0";
            // 
            // missesp2
            // 
            this.missesp2.BackColor = System.Drawing.Color.Transparent;
            this.missesp2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.missesp2.ForeColor = System.Drawing.Color.ForestGreen;
            this.missesp2.Location = new System.Drawing.Point(115, 213);
            this.missesp2.Name = "missesp2";
            this.missesp2.Size = new System.Drawing.Size(23, 35);
            this.missesp2.TabIndex = 4;
            this.missesp2.Text = "0";
            // 
            // missesp1
            // 
            this.missesp1.BackColor = System.Drawing.Color.Transparent;
            this.missesp1.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.missesp1.ForeColor = System.Drawing.Color.ForestGreen;
            this.missesp1.Location = new System.Drawing.Point(98, 213);
            this.missesp1.Name = "missesp1";
            this.missesp1.Size = new System.Drawing.Size(23, 35);
            this.missesp1.TabIndex = 5;
            this.missesp1.Text = "0";
            // 
            // hpp1
            // 
            this.hpp1.BackColor = System.Drawing.Color.Transparent;
            this.hpp1.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hpp1.ForeColor = System.Drawing.Color.ForestGreen;
            this.hpp1.Location = new System.Drawing.Point(97, 303);
            this.hpp1.Name = "hpp1";
            this.hpp1.Size = new System.Drawing.Size(23, 35);
            this.hpp1.TabIndex = 6;
            this.hpp1.Text = "0";
            // 
            // hpp2
            // 
            this.hpp2.BackColor = System.Drawing.Color.Transparent;
            this.hpp2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hpp2.ForeColor = System.Drawing.Color.ForestGreen;
            this.hpp2.Location = new System.Drawing.Point(116, 303);
            this.hpp2.Name = "hpp2";
            this.hpp2.Size = new System.Drawing.Size(23, 35);
            this.hpp2.TabIndex = 7;
            this.hpp2.Text = "0";
            // 
            // hpp3
            // 
            this.hpp3.BackColor = System.Drawing.Color.Transparent;
            this.hpp3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hpp3.ForeColor = System.Drawing.Color.ForestGreen;
            this.hpp3.Location = new System.Drawing.Point(134, 303);
            this.hpp3.Name = "hpp3";
            this.hpp3.Size = new System.Drawing.Size(31, 35);
            this.hpp3.TabIndex = 8;
            this.hpp3.Text = "0";
            // 
            // hitsc1
            // 
            this.hitsc1.BackColor = System.Drawing.Color.Transparent;
            this.hitsc1.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hitsc1.ForeColor = System.Drawing.Color.ForestGreen;
            this.hitsc1.Location = new System.Drawing.Point(550, 116);
            this.hitsc1.Name = "hitsc1";
            this.hitsc1.Size = new System.Drawing.Size(23, 35);
            this.hitsc1.TabIndex = 9;
            this.hitsc1.Text = "0";
            // 
            // hitsc2
            // 
            this.hitsc2.BackColor = System.Drawing.Color.Transparent;
            this.hitsc2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hitsc2.ForeColor = System.Drawing.Color.ForestGreen;
            this.hitsc2.Location = new System.Drawing.Point(568, 116);
            this.hitsc2.Name = "hitsc2";
            this.hitsc2.Size = new System.Drawing.Size(23, 35);
            this.hitsc2.TabIndex = 10;
            this.hitsc2.Text = "0";
            // 
            // hitsc3
            // 
            this.hitsc3.BackColor = System.Drawing.Color.Transparent;
            this.hitsc3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hitsc3.ForeColor = System.Drawing.Color.ForestGreen;
            this.hitsc3.Location = new System.Drawing.Point(586, 116);
            this.hitsc3.Name = "hitsc3";
            this.hitsc3.Size = new System.Drawing.Size(23, 35);
            this.hitsc3.TabIndex = 11;
            this.hitsc3.Text = "0";
            // 
            // missesc1
            // 
            this.missesc1.BackColor = System.Drawing.Color.Transparent;
            this.missesc1.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.missesc1.ForeColor = System.Drawing.Color.ForestGreen;
            this.missesc1.Location = new System.Drawing.Point(550, 208);
            this.missesc1.Name = "missesc1";
            this.missesc1.Size = new System.Drawing.Size(23, 35);
            this.missesc1.TabIndex = 12;
            this.missesc1.Text = "0";
            // 
            // missesc2
            // 
            this.missesc2.BackColor = System.Drawing.Color.Transparent;
            this.missesc2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.missesc2.ForeColor = System.Drawing.Color.ForestGreen;
            this.missesc2.Location = new System.Drawing.Point(569, 208);
            this.missesc2.Name = "missesc2";
            this.missesc2.Size = new System.Drawing.Size(23, 35);
            this.missesc2.TabIndex = 13;
            this.missesc2.Text = "0";
            // 
            // missesc3
            // 
            this.missesc3.BackColor = System.Drawing.Color.Transparent;
            this.missesc3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.missesc3.ForeColor = System.Drawing.Color.ForestGreen;
            this.missesc3.Location = new System.Drawing.Point(587, 208);
            this.missesc3.Name = "missesc3";
            this.missesc3.Size = new System.Drawing.Size(23, 35);
            this.missesc3.TabIndex = 14;
            this.missesc3.Text = "0";
            // 
            // hpc1
            // 
            this.hpc1.BackColor = System.Drawing.Color.Transparent;
            this.hpc1.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hpc1.ForeColor = System.Drawing.Color.ForestGreen;
            this.hpc1.Location = new System.Drawing.Point(549, 304);
            this.hpc1.Name = "hpc1";
            this.hpc1.Size = new System.Drawing.Size(23, 35);
            this.hpc1.TabIndex = 15;
            this.hpc1.Text = "0";
            // 
            // hpc2
            // 
            this.hpc2.BackColor = System.Drawing.Color.Transparent;
            this.hpc2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hpc2.ForeColor = System.Drawing.Color.ForestGreen;
            this.hpc2.Location = new System.Drawing.Point(567, 304);
            this.hpc2.Name = "hpc2";
            this.hpc2.Size = new System.Drawing.Size(23, 35);
            this.hpc2.TabIndex = 16;
            this.hpc2.Text = "0";
            // 
            // hpc3
            // 
            this.hpc3.BackColor = System.Drawing.Color.Transparent;
            this.hpc3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hpc3.ForeColor = System.Drawing.Color.ForestGreen;
            this.hpc3.Location = new System.Drawing.Point(586, 304);
            this.hpc3.Name = "hpc3";
            this.hpc3.Size = new System.Drawing.Size(23, 35);
            this.hpc3.TabIndex = 17;
            this.hpc3.Text = "0";
            // 
            // winnerlabel
            // 
            this.winnerlabel.BackColor = System.Drawing.Color.Black;
            this.winnerlabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.winnerlabel.Font = new System.Drawing.Font("Bodoni MT Condensed", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.winnerlabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.winnerlabel.Location = new System.Drawing.Point(310, 22);
            this.winnerlabel.Name = "winnerlabel";
            this.winnerlabel.Size = new System.Drawing.Size(204, 34);
            this.winnerlabel.TabIndex = 18;
            this.winnerlabel.Text = "WINNER";
            this.winnerlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // newGame
            // 
            this.newGame.Location = new System.Drawing.Point(103, 360);
            this.newGame.Name = "newGame";
            this.newGame.Size = new System.Drawing.Size(75, 23);
            this.newGame.TabIndex = 19;
            this.newGame.Text = "New Game";
            this.newGame.UseVisualStyleBackColor = true;
            this.newGame.Click += new System.EventHandler(this.newGame_Click);
            // 
            // sendData
            // 
            this.sendData.Location = new System.Drawing.Point(401, 360);
            this.sendData.Name = "sendData";
            this.sendData.Size = new System.Drawing.Size(75, 23);
            this.sendData.TabIndex = 20;
            this.sendData.Text = "Send Data";
            this.sendData.UseVisualStyleBackColor = true;
            this.sendData.Click += new System.EventHandler(this.sendData_Click);
            // 
            // mainMenu
            // 
            this.mainMenu.Location = new System.Drawing.Point(245, 360);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(75, 23);
            this.mainMenu.TabIndex = 21;
            this.mainMenu.Text = "Main Menu";
            this.mainMenu.UseVisualStyleBackColor = true;
            this.mainMenu.Click += new System.EventHandler(this.mainMenu_Click);
            // 
            // Quit
            // 
            this.Quit.Location = new System.Drawing.Point(534, 360);
            this.Quit.Name = "Quit";
            this.Quit.Size = new System.Drawing.Size(75, 23);
            this.Quit.TabIndex = 22;
            this.Quit.Text = "Quit";
            this.Quit.UseVisualStyleBackColor = true;
            this.Quit.Click += new System.EventHandler(this.Quit_Click);
            // 
            // Score
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::INDES_T3.Properties.Resources.Score;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(718, 397);
            this.Controls.Add(this.Quit);
            this.Controls.Add(this.mainMenu);
            this.Controls.Add(this.sendData);
            this.Controls.Add(this.newGame);
            this.Controls.Add(this.winnerlabel);
            this.Controls.Add(this.hpc3);
            this.Controls.Add(this.hpc2);
            this.Controls.Add(this.hpc1);
            this.Controls.Add(this.missesc3);
            this.Controls.Add(this.missesc2);
            this.Controls.Add(this.missesc1);
            this.Controls.Add(this.hitsc3);
            this.Controls.Add(this.hitsc2);
            this.Controls.Add(this.hitsc1);
            this.Controls.Add(this.hpp3);
            this.Controls.Add(this.hpp2);
            this.Controls.Add(this.hpp1);
            this.Controls.Add(this.missesp1);
            this.Controls.Add(this.missesp2);
            this.Controls.Add(this.missesp3);
            this.Controls.Add(this.hitsp3);
            this.Controls.Add(this.hitsp2);
            this.Controls.Add(this.hitsp1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Score";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "INDES: Battleships (Score)";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label hitsp1;
        private System.Windows.Forms.Label hitsp2;
        private System.Windows.Forms.Label hitsp3;
        private System.Windows.Forms.Label missesp3;
        private System.Windows.Forms.Label missesp2;
        private System.Windows.Forms.Label missesp1;
        private System.Windows.Forms.Label hpp1;
        private System.Windows.Forms.Label hpp2;
        private System.Windows.Forms.Label hpp3;
        private System.Windows.Forms.Label hitsc1;
        private System.Windows.Forms.Label hitsc2;
        private System.Windows.Forms.Label hitsc3;
        private System.Windows.Forms.Label missesc1;
        private System.Windows.Forms.Label missesc2;
        private System.Windows.Forms.Label missesc3;
        private System.Windows.Forms.Label hpc1;
        private System.Windows.Forms.Label hpc2;
        private System.Windows.Forms.Label hpc3;
        private System.Windows.Forms.Label winnerlabel;
        private System.Windows.Forms.Button newGame;
        private System.Windows.Forms.Button sendData;
        private System.Windows.Forms.Button mainMenu;
        private System.Windows.Forms.Button Quit;
    }
}